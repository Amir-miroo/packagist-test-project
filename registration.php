<?php
/**
 * @Author a b
 * @Package Prince_Test1
 * @Copyright Copyright (c) 2021
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Prince_Test1',
    __DIR__
);